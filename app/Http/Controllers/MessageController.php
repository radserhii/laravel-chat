<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use Illuminate\Http\Request;
use App\Message;

class MessageController extends Controller
{
    public function index()
    {
        return view('message.index');
    }

    public function get()
    {
        $messages = Message::take(200)->pluck('content');
        return $messages;
    }

    public function store()
    {
        $message = new Message();
        $content = request('message');
        $message->content = $content;
        $message->save();

        event(new MessageSent($content));

        return $content;
    }
}